import os
from collections import deque

class Graph(object):
    # Initializing empty graph
    def __init__(self):
        self.adj_list = dict()    # Initial adjacency list is empty dictionary
        self.vertices = set()    # Vertices are stored in a set
        self.degrees = dict()    # Degrees stored as dictionary

    # Checks if (node1, node2) is edge of graph. Output is 1 (yes) or 0 (no).
    def isEdge(self,node1,node2):
        if node1 in self.vertices:        # Check if node1 is vertex
            if node2 in self.adj_list[node1]:    # Then check if node2 is neighbor of node1
                return 1            # Edge is present!

        if node2 in self.vertices:        # Check if node2 is vertex
            if node1 in self.adj_list[node2]:    # Then check if node1 is neighbor of node2
                return 1            # Edge is present!

        return 0                # Edge not present!

    # Add undirected, simple edge (node1, node2)
    def addEdge(self,node1,node2):

        # print('Called')
        if node1 == node2:            # Self loop, so do nothing
            # print('self loop')
            return
        if node1 in self.vertices:        # Check if node1 is vertex
            nbrs = self.adj_list[node1]        # nbrs is neighbor list of node1
            if node2 not in nbrs:         # Check if node2 already neighbor of node1
                nbrs.add(node2)            # Add node2 to this list
                self.degrees[node1] = self.degrees[node1]+1    # Increment degree of node1

        else:                    # So node1 is not vertex
            self.vertices.add(node1)        # Add node1 to vertices
            self.adj_list[node1] = {node2}    # Initialize node1's list to have node2
            self.degrees[node1] = 1         # Set degree of node1 to be 1

        if node2 in self.vertices:        # Check if node2 is vertex
            nbrs = self.adj_list[node2]        # nbrs is neighbor list of node2
            if node1 not in nbrs:         # Check if node1 already neighbor of node2
                nbrs.add(node1)            # Add node1 to this list
                self.degrees[node2] = self.degrees[node2]+1    # Increment degree of node2

        else:                    # So node2 is not vertex
            self.vertices.add(node2)        # Add node2 to vertices
            self.adj_list[node2] = {node1}    # Initialize node2's list to have node1
            self.degrees[node2] = 1         # Set degree of node2 to be 1

    # Give the size of the graph. Outputs [vertices edges wedges]
    #
    def size(self):
        n = len(self.vertices)            # Number of vertices

        m = 0                    # Initialize edges/wedges = 0
        wedge = 0
        for node in self.vertices:        # Loop over nodes
            deg = self.degrees[node]      # Get degree of node
            m = m + deg             # Add degree to current edge count
            wedge = wedge+deg*(deg-1)/2        # Add wedges centered at node to wedge count
        return [n, m, wedge]            # Return size info

    # Print the graph
    def output(self,fname,dirname):
        os.chdir(dirname)
        f_output = open(fname,'w')

        for node1 in list(self.adj_list.keys()):
            f_output.write(str(node1)+': ')
            for node2 in (self.adj_list)[node1]:
                f_output.write(str(node2)+' ')
            f_output.write('\n')
        f_output.write('------------------\n')
        f_output.close()

    def path(self, src, dest):
        """ implement your shortest path function here """
        shortest_path = []

        # Your code comes here
        dist, pred, VISITED = self.BFS(src) # we run BFS and set variables
        if dest in VISITED: # if dest was visited in BFS 
            curr = dest # helpful naming variable
            while pred[curr] != None: # Basically while curr isn't src
                shortest_path.append(curr) # append curr to shortest_path
                curr = pred[curr] # Move curr "pointer"
            shortest_path.append(src) # append the src to the end
            
        shortest_path.reverse() # reverse the array to go from src -> dest
        return shortest_path # return array

    def levels(self, src):
        """ implement your level set code here """
        level_sizes = []

        # Your code comes in here
        dist, pred, visited = self.BFS(src) #run BFS, set variables
        
        #really lame code here...but it works
        i =j = k = l = m = n = o = 0
        
        #Check the distances from the source
        for v in pred:
            if dist[v] == 0:
                i += 1 #increment the number of nodes dist x from source
            elif dist[v] == 1:
                j += 1
            elif dist[v] == 2:
                k += 1
            elif dist[v] == 3:
                l += 1
            elif dist[v] == 4:
                m += 1
            elif dist[v] == 5:
                n += 1
            else:
                o += 1
                #print(v) <- used to find specific thespians with at least 6 degrees
        
        #append all the values to level_sizes     
        level_sizes.append(i)
        level_sizes.append(j)
        level_sizes.append(k)
        level_sizes.append(l)
        level_sizes.append(m)
        level_sizes.append(n)
        level_sizes.append(o)
        
        return level_sizes

    def BFS(self, src):
        """BFS code grabbed and modified from class lecture"""
        infinity = float('inf')
        #create an empty queue, a set, and two empty dictionaries 
        Q = deque() 
        VISITED = set([src])
        dist = {}
        pred = {}
        for v in self.vertices:
            dist[v] = infinity
        dist[src] = 0  # I set this below the for loop to avoid a weird issue with levels
        pred[src] = None # set predecessor to None
        Q.append(src)
        while len(Q) is not 0: # while the queue is not empty
            u = Q.popleft() #pop the end of the queue for this structure
            for v in self.adj_list.get(u): # adjacency list of u
                if v not in VISITED:
                    VISITED.add(v)
                    pred[v] = u
                    dist[v] = dist[u] + 1
                    Q.append(v) # add into the queue
        return dist, pred, VISITED
        

