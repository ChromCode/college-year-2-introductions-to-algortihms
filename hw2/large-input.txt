MergeSort large-input run time: Merge Sort sorted an array of size 10^6 in under
60 sec on average.

SelectionSort and InsertionSort large-input run time: Both of these sorting algorithms
couldn't sort an array of size 10^6 in under an hour.