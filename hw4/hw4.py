import numpy as np
import timeit
import matplotlib.pyplot as plt

#Based on Equation(1)
def matmult(A, x):
    B = np.empty([ A.shape[0] ])
    #from 1 to n in the rows of array A
    for i in range(A.shape[0]):
        sum = 0
        #from 1 to n in the rows of vector x
        for j in range(0, x.shape[0]):
            #take the product of A[i,j] and x[j]
            mult = A[i][j] * x[j]
            #then add all of the products for j
            sum = sum + mult
        #set a new array to have elements of the sum
        B[i] = sum
    return B

#base#d on Equation(2)
def hadmatmult(H, x):
    n = len(x)//2
    #if vector length is 0 return empty matrix
    if len(x) == 1:
        return np.array([H[0][0]*x[0]])
    else:
        #split matrix accordingly to half vector length
        half = H[0:n][0:n]
        m1 = hadmatmult(half,x[:n])
        m2 = hadmatmult(half,x[n:])
    #add/substract values accordingly
    return np.concatenate((np.add(m1,m2),np.subtract(m1,m2)), axis = 0)

def hadmat(k):
    n = 1;
    #2^k value for matrix
    for i in range(k):
        n *=2
    #set empty matrix
    H = np.zeros((n,n))
    #H = [[0 for x in range(n)] for y in range(n)];
    a = 1
    #initialize matrix of size n
    while a < n:
        for b in range(a):
            for c in range(a):
                #set matrix values according for matrix values
                H[b+a][c]    = H[b][c]
                H[b][c+a]    = H[b][c]
                H[b+a][c+a]  = not H[b][c]
        a += a
    #write matrix
    for i in range(n):
        for j in range(n):
            if H[i][j]:
                H[i-n][j-n] = -1
            else:
                H[i][j] = 1
    return H
