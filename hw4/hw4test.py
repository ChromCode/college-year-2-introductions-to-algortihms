import hw4
import numpy as np
import numpy.testing as nt
from scipy import linalg
import unittest
import os.path

class Hw4Test(unittest.TestCase):
    def setUp(self):
        self.nxn= [
            np.random.randint(-5,50,[2,2]),
            np.random.randint(-5,50,[4,4]),
            np.random.randint(-5,50,[8,8]),
            np.random.randint(-5,50,[16,16])
        ]
        self.had = [
            linalg.hadamard(2),
            linalg.hadamard(4),
            linalg.hadamard(8),
            linalg.hadamard(16)
        ]
        self.vec = [
            np.random.randint(-5,50,[2]),
            np.random.randint(-5,50,[4]),
            np.random.randint(-5,50,[8]),
            np.random.randint(-5,50,[16])
        ]

    def testMatMult(self):
        """" Problem 1: matmult(A,x).

        Multiply a matrix with a vector."""
        for t in range(4):
            nt.assert_equal(hw4.matmult(self.nxn[t],self.vec[t]),
                            self.nxn[t].dot(self.vec[t]))

    def testHadMat(self):
        """ Problem 2: hadmat(k).

        Create hadamard of size 2^k calls student hadmat with t=1,2,3,4
        it is supposed to generate hadmat of size 2,4,8,16"""
        for t in range(4):
            nt.assert_equal(hw4.hadmat(t+1), self.had[t])

    def testHadMatMult(self):
        """ Problem 4: hadmatmult(H,x).

        Takes hadamart H and self.vector x, multiplies."""
        for t in range(4):
            nt.assert_equal(hw4.hadmatmult(self.had[t],self.vec[t]),
                            self.had[t].dot(self.vec[t]))

    @unittest.expectedFailure
    def testEfficientMatMult(self):
        """ Bonus problem: efficienthadmatmult(x) """
        for t in range(4):
            nt.assert_equal(hw4.efficienthadmatmult(self.vec[t]),
                            self.had[t].dot(self.vec[t]))

if __name__ == '__main__':
    #Assignment 4 file checks
    filesok=True
    if not os.path.isfile("../partners.txt"):
        filesok=False        
        print("\nCannot find ../partners.txt.\n")
    if not os.path.isfile("matmulttime.pdf"):
        filesok=False        
        print("\nCannot find matmulttime.pdf.\n")
    if not os.path.isfile("analysis.pdf"):
        filesok=False        
        print("\nCannot find analysis.pdf\n")
    if filesok:
        print("\nSeems like you have all your files placed correctly!\n")

    unittest.main(exit=False)