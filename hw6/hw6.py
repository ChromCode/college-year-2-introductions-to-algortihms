from heapq import nlargest, heapify, heappush
import sys

class Node(object):

        def __init__(self, key, value):
            """Create a new leaf with key and value"""
            self.value = value
            self.key = key
            
            #Node pointers
            self.parent = None
            self.left = None
            self.right = None
            
class BSTree(object):

    def __init__(self):
        self.root = Node(None, None)
         
    def insert(self, key, value):
        """Insert a node with key and value into the tree"""
        newNode = Node(key, value)
        if (self.root.key is None): # check if tree is empty
            self.root = newNode # set root to be the newNode
            newNode.parent = None
        else:
            current = self.root
            parent = Node(None, None)
            while(True): # exits internally
                parent = current
                if(key < current.key and current.key is not None): #go left
                    current = current.left
                    if(current is None):  # if end of the path
                        parent.left = newNode # insert on parent.left
                        newNode.parent = parent # set Node parent
                        return
                elif(key > current.key and current.key is not None): # go right
                    current = current.right
                    if(current is None): # if end of the path
                        parent.right = newNode # insert on parent.right
                        newNode.parent = parent #set Node parent
                        return
                else: # if already in the tree, increment value
                    current.value += 1 
                    return 
                    
    def delete(self, key):
        """Delete the node with the key """
        nodeToDelete = self.find(key) # First find the node
        if nodeToDelete is not None:
            self.deleteNode(nodeToDelete) # pass it off to a helper function
    
    def find(self, key):
        """Find a node in the tree with key, otherwise return false"""
        currentNode = self.root # start at the root
        while currentNode is not None:
            if key == currentNode.key:
                return currentNode
            elif key < currentNode.key: #key is less than current: go left
                currentNode = currentNode.left
            else:
                currentNode = currentNode.right #key is greater than current: go right
        return None #return None if not found in the tree
            
    def successor(self, key):
        """Find the successor to the node with key"""
        currentNode = self.find(key)
        if currentNode.right is not None:
            minNode = findMin(currentNode.right) #find min in the tree
            return minNode
        currentParent = currentNode.parent #if node is a leaf
        while currentParent is not None and currentNode == currentParent.right:
            currentNode = currentParent
            currentParent = currentParent.parent
        return currentParent
        
    def inOrderTraversal(self):
        """Print the inOrderTraversal in sorted Order"""
        self.outputHelper(self.root) #pass is off to a helper function
        
    def outputHelper(self, Node):
        """Recursive algorithm for inOrder, uses a node as input, will be the root"""
        if(Node is not None):
            self.outputHelper(Node.left)
            print(Node.key, Node.value)
            self.outputHelper(Node.right)
            
    def findMin(self, x):
        """Find the min node from a node rooted at x"""
        while x.left is not None: #Keep going until x.left is None
            x = x.left
        return x
        
    def swapNodes(self, node, new_node):
        """Replace/swap the nodes to fit delete cases"""
        # root case
        if node == self.root:
            self.root = new_node
            return
        parent = node.parent
        if parent.left is not None and parent.left == node:
            parent.left = new_node
        elif parent.right is not None and parent.right == node:
            parent.right = new_node
        else:
            #we have an error
            raise RuntimeError

    def deleteNode(self, node):
        """Delete the node found with the key"""
        if node.left and node.right: # the node has two children
            # Find successor
            successor = self.findMin(node.right)
            node.key = successor.key  # Copy the node
            node.value = successor.value
            # Remove the successor
            self.deleteNode(successor)
        elif node.left: # 1 left child
            self.swapNodes(node, node.left)
        elif node.right: # 1 right child
            self.swapNodes(node, node.right)
        else: # no children
            self.swapNodes(node, None)